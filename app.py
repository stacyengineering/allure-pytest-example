import logging
import sys

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.DEBUG)
stdout_handler.setFormatter(formatter)

file_handler = logging.FileHandler('exec.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)


logger.addHandler(file_handler)
logger.addHandler(stdout_handler)


class App:
    def __init__(self):
        self.logged_in = False
        self.users = []

    def login(self, username, password):
        logger.info(f"Logging in with credentials: {username} / {password}")
        if username not in ["admin", "regular"]:
            logger.error("Username is incorrect")
            raise Exception("Username is incorrect!")
        if password != "welcome123":
            logger.error("Password is incorrect")
            raise Exception("Password is incorrect!")
        if username == "regular":
            self.users.append("Bob")
        self.logged_in = True
        return True

    def get_users(self):
        logger.info("Getting users")
        if not self.logged_in:
            logger.error("Authentication error")
            raise Exception("Authentication error!")
        return self.users
