from tests.test_base import TestBase
import pathlib

import allure
import allure_pytest
# from allure import label
# from allure import severity
# from allure import tag
# from allure import epic, feature, story, link
# from allure import link
# from allure import issue, testcase
# from allure_commons._allure import Dynamic as dynamic
# from allure import step
# from allure import attach
# from allure_commons.types import Severity as severity_level
# from allure_commons.types import AttachmentType as attachment_type


class TestLogin(TestBase):

    @allure.story("https://jira.com/issue/DS-123")
    @allure.link("https://jira.com/issue/DS-123")
    @allure.title("Test login with correct credentials")
    def test_login_successful(self):
        """Verify successful login"""
        with allure.step("Login into the app"):
            allure.attach.file(self.path_to_logs, attachment_type=allure.attachment_type.PNG)
            self.app.login("admin", "welcome123")
        with allure.step("Verify user is logged in"):
            assert self.app.logged_in
        with allure.step("Verify users list can be retrieved and is empty"):
            assert self.app.get_users() == []

    @allure.story("https://jira.com/issue/DS-124")
    @allure.link("https://jira.com/issue/DS-123")
    @allure.title("Test login with correct credentials (version 2 - step decorators)")
    def test_login_successful_step_decorators(self):
        """Verify successful login"""
        self.app_handler.login("admin", "welcome123")
        result = self.app_handler.get_users()
        assert result == []

    @allure.story("https://jira.com/issue/DS-123")
    @allure.link("https://jira.com/issue/DS-123")
    @allure.title("Test login with correct credentials (version 3) - custom verifiers")
    def test_login_successful_steps(self):
        """Verify successful login"""
        self.app_handler.login("admin", "welcome123")
        self.verify.true(self.app.logged_in, "Verify user is logged in")
        result = self.app_handler.get_users()
        self.verify.true(result == [], "Verify users list can be retrieved and is empty")

    @allure.story("https://jira.com/issue/DS-123")
    @allure.link("https://jira.com/issue/DS-123")
    @allure.title("Test login with regular users")
    def test_login_unsuccessful(self):
        """Verify login with regular user"""
        self.app_handler.login("regular", "welcome123")
        result = self.app_handler.get_users()
        self.verify.true(result == [], "Verify users list can be retrieved and is empty")
