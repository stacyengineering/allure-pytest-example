import allure

from functools import partial, update_wrapper


class Reporter:
    def __init__(self, app_log_file):
        self.log_file = app_log_file

    def __basic_assert(self, assertion_executable, msg):
        with allure.step(msg):
            try:
                assertion_executable()
            except AssertionError as e:
                allure.attach.file(self.log_file, attachment_type=allure.attachment_type.TEXT)
                raise e

    def __get_assertion_executable(self, expression, msg):
        assert expression, msg

    def true(self, assertion, msg):
        self.__basic_assert(partial(self.__get_assertion_executable, assertion, msg), msg)

    def not_fails(self, operation, msg):
        self.__basic_assert(operation, msg)
