import allure


class AppHandler:
    def __init__(self, app):
        self._app = app

    @allure.step("Login into the app")
    def login(self, username, password):
        return self._app.login(username, password)

    @allure.step("Get users")
    def get_users(self):
        return self._app.get_users()
