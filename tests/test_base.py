import pathlib
from unittest import TestCase

from app import App
from tests.base.app_handler import AppHandler
from tests.base.reporting import Reporter


class TestBase(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.app = App()
        cls.path_to_logs = pathlib.Path(pathlib.Path(".").parent.parent / "exec.log")
        cls.app_handler = AppHandler(cls.app)
        cls.verify = Reporter(cls.path_to_logs)
